import re
from urllib import unquote
import logging

class GoogleMapCore:
    __GoogleMapRe = None

    def __init__(self):
        if self.__GoogleMapRe is None:
            self.__GoogleMapRe = re.compile(r"""^https?\:\/\/(www\.|maps\.)?google(\.[a-z]+){1,2}\/maps\/""")

    def getShortContent(self, Content):
        ret = ''
        isGoogleMap = self.isGoogleMap(Content)
        if isGoogleMap == True:
            ret = self.__getAddress(Content)
        return ret 

    def isGoogleMap(self, Content):
        ret = None
        ret = self.__GoogleMapRe.match(Content)
        return False if ret is None else True

    def __getAddress(self, Content):
        ret = ''
        address = None
        temp = Content.split('/')
        if len(temp) > 5 and temp[4] == 'place':
            address = temp[5]
            address = unquote(address).encode('latin-1').decode('utf-8')
            temp = address.split('+')
            for w in temp:
                ret += w.capitalize()
        return ret

