import hashlib
import random
import time
import logging

class RandomCore:
    def __init__(self, WordList):
        try:
            if True == isinstance(WordList, list) and len(WordList) > 0:
                self.WordList = WordList
        except:
            raise Exception("WordList should be a non-empty list")

    def getShortContent(self, Content):
        Timestamp = int(time.time())
        Digest = self.getContentDigest(Content)
        Key = self.WordList[(Digest + random.randint(0, Timestamp)) % len(self.WordList)]
        return Key.capitalize()

    def getContentDigest(self, Content):
        return int(hashlib.sha256(Content.encode('utf-8')).hexdigest(), 16)

    def getRandomNumber(self, upper):
        if isinstance( upper, int ) != True or upper == 0:
            upper = 1 
        Timestamp = int(time.time())
        return random.randint(0, Timestamp) % upper
