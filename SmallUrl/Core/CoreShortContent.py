import RandomCore
import GoogleMapCore 
import TextCore 
import logging

class CoreShortContent:
    __Random = []
    __Map = None
    __Text = None

    def __init__(self, WordList):
        try:
            if len(self.__Random) == 0:
                self.__Random.append(RandomCore.RandomCore(WordList[0]))
                self.__Random.append(RandomCore.RandomCore(WordList[1]))
                self.__Random.append(RandomCore.RandomCore(WordList[2]))
        except:
            raise Exception("RandomCore initial fail")
        try:
            if self.__Map is None:
                self.__Map = GoogleMapCore.GoogleMapCore()
        except:
            raise Exception("GoogleMapCore initial fail")
        try:
            if self.__Text is None:
                self.__Text = TextCore.TextCore()
        except:
            raise Exception("GoogleMapCore initial fail")

    def getRandomShortContent(self, Content):
        ret = ""
        for KG in self.__Random:
            ret += KG.getShortContent(Content)
        return ret
    
    def getGoogleMapShortContent(self, Content):
        ret = ""
        ret = self.__Map.getShortContent(Content)
        return ret

    def getRandomNumber(self, upper):
        rand = self.__Random[0].getRandomNumber(upper)
        return str(rand)

    def isGoogleMap(self, Content):
        return self.__Map.isGoogleMap(Content)

    def isText(self, Content):
        return self.__Text.isText(Content)
        

