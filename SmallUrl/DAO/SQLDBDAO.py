import logging
import sys;
import cymysql; #mysql library (you will need to install this on the system)

#MySQL Singleton Class
class MySQLConnector:
    _connection = None
    _instance = None
    dbhost = None
    dblogin = None
    dbpassword = None
    dbname = None
    charset = None

    def __init__(self, dbhost, dblogin, dbpassword, dbname, charset):
        self.dbhost = dbhost
        self.dblogin = dblogin
        self.dbpassword = dbpassword
        self.dbname = dbname
        self.charset = charset
        try:
            if MySQLConnector._instance == None:
                MySQLConnector._instance = self
                MySQLConnector._instance.connect()
        except Exception as e:
            print("MySQL Error "+str(e))

    def instance(self):
        return MySQLConnector._instance

    def get_connection(self):
        return MySQLConnector._connection

    def connect(self, debug=False):
        try:
            if MySQLConnector._connection is None:
                MySQLConnector._connection = cymysql.connect(host=self.dbhost, user=self.dblogin, passwd=self.dbpassword, db=self.dbname, charset=self.charset)
                if debug:
                    logging.info("INFO: Database connection successfully established")
        except Exception as e:
            logging.error("Cnnection Couldn't be created... Fatal Error! "+str(e))
            sys.exit()

    def disconnect(self):
        try:
            MySQLConnector._connection.close()
        except:
            pass;#connection not open

    #returns escaped data for insertion into mysql
    def esc(self, esc):
        return cymysql.escape_string(str(esc))

    #query with no result returned
    def query(self, sql):
        self.__reConnect()
        cur = MySQLConnector._connection.cursor()
        return cur.execute(sql)

    def tryquery(self, sql):
        try:
            cur = MySQLConnector._connection.cursor();
            return cur.execute(sql)
        except:
            return False

    #inserts and returns the inserted row id (last row id in PHP version)
    def insert(self, sql):
        cur = MySQLConnector._connection.cursor()
        cur.execute(sql)
        return self._connection.insert_id()

    def tryinsert(self, sql):
        try:
            cur = MySQLConnector._connection.cursor()
            cur.execute(sql)
            return self._connection.insert_id()
        except:
            return -1;

    #returns the first item of data
    def queryrow(self, sql):
        cur = MySQLConnector._connection.cursor()
        cur.execute(sql);
        return cur.fetchone()

    #returns a list of data (array)
    def queryrows(self, sql):
        cur = MySQLConnector._connection.cursor();
        cur.execute(sql);
        return cur.fetchmany()

    def __reConnect(self):
        try:
            MySQLConnector._instance.ping()
        except cymysql.err.OperationalError as e:
            if e[0] == 2013: # Lost connection to server
                MySQLConnector._instance.connect()
                logging.info("reconnect db")
            # redo open_connection and do_something
            else:
                raise e

#end class MySQLConnector
