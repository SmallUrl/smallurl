import pycurl
import StringIO
import certifi

class WebsiteDAO:
    _curl = None
    _contents = None
    _buffer = None
    _agent = 'Opera/9.80 (Windows NT 5.1; U; cs) Presto/2.2.15 Version/10.00'
 
    def __init__ (self):
        if self._curl is None:
            self._curl = pycurl.Curl()
        self._buffer = StringIO.StringIO();

    def ReadPage (self, url):
        self.contents = None
        self._buffer = StringIO.StringIO();
        self._curl.setopt(pycurl.CAINFO, certifi.where())
        self._curl.setopt(self._curl.URL, url)
        self._curl.setopt(self._curl.USERAGENT, self._agent)
        self._curl.setopt(self._curl.WRITEFUNCTION, self._buffer.write)
        self._curl.setopt(self._curl.SSL_VERIFYPEER, 1) 
        self._curl.setopt(self._curl.SSL_VERIFYHOST, 2)
        self._curl.perform()
        self.contents = self._buffer.getvalue()
        self._buffer.close()
        return self.contents
    
    def ReadHeader (self, url):
        header = None
        buf = StringIO.StringIO()
        self._curl.setopt(self._curl.URL, url)
        self._curl.setopt(self._curl.USERAGENT, self._agent)
        self._curl.setopt(self._curl.HEADERFUNCTION, buf.write)
        self._curl.setopt(self._curl.SSL_VERIFYPEER, 1) 
        self._curl.setopt(self._curl.SSL_VERIFYHOST, 2)
        self._curl.perform()
        header = buf.getvalue()
        buf.close()
        return header

