import tornado.httpserver
import tornado.ioloop
import tornado.web
import sys
reload(sys)
sys.setdefaultencoding('UTF-8')
import os
import logging
#import socket
#import time
import uuid
import json
from urllib import unquote

sys.path.append("./BO")
from SmallUrlBO import SmallUrlBO
from WhitelistBO  import WhitelistBO
from SmallUrlStatistics import SmallUrlStatistics

from tornado.options import define, options

# if we did not get arguments from command line we can get default values
define("port", default=80, help="run on the given port", type=int)
define("db_host", default="", help="database host")
define("db_database", default="", help="database name")
define("db_user", default="", help="database user")
define("db_password", default="", help="database password")

class Application(tornado.web.Application):
    def __init__(self):
        handlers = [
            (r"^/", ShortenHandler),
            (r"^/STATS", StatsHandler),
            (r"^/TUTORIAL", TutorialHandler),
            (r"^/openapi/v1/convert", ApiConvertHandler),
            (r"^/[a-zA-Z0-9%+-_()]+", RedirectHandler)
        ]

        settings = dict(
            template_path=os.path.join(os.path.dirname(__file__), "templates"),
            static_path=os.path.join(os.path.dirname(__file__), "static"),
            debug=True,
        )

        self.config = {
            "name": os.path.join(os.path.dirname(__file__), """data/name_le8.csv.txt"""),
            "verb": os.path.join(os.path.dirname(__file__), """data/verb_le8.csv.txt"""),
            "noun": os.path.join(os.path.dirname(__file__), """data/noun_le8.csv.txt"""),
            "whitelist": os.path.join(os.path.dirname(__file__), """data/whitelist.txt"""),
            "dbhost": options.db_host,
            "dblogin": options.db_user,
            "dbpassword": options.db_password,
            "dbname": options.db_database,
            "charset": 'utf8',
            "cookieName": 'user',
        }

        tornado.web.Application.__init__(self, handlers, **settings)

class BaseHandler(tornado.web.RequestHandler):
    def addHSTS(self):
        self.add_header("Strict-Transport-Security", "max-age=31536000; includeSubDomains; preload")

class StatsHandler(BaseHandler):
    def get(self):
        self.addHSTS()
        SmallStats = SmallUrlStatistics(self.application.config)
        total_accessor_count = SmallStats.get_total_accesor_count()
        total_data_count     = SmallStats.get_total_data_count()
        self.render("stats.html", total_accessor_count, total_data_count)


class TutorialHandler(BaseHandler):
    def get(self):
        self.addHSTS()
        self.render("tutorial.html")

    def post(self):
        self.addHSTS()
        self.render("tutorial.html")


class ApiConvertHandler(BaseHandler):
    def get(self):
        self.post()

    def post(self):
        Whitelist = WhitelistBO(self.application.config)

        try:
            User = self.get_argument("id")
        except Exception as e:
            msg = {"result":"fail", "info":"permission deny"}
            self.write(json.dumps(msg))
            return

        if Whitelist.isInWhitelist(User) == False:
            msg = {"result":"fail", "info":"permission deny"}
            self.write(json.dumps(msg))
            return

        try:
            LongData = self.get_argument("long_data")
        except Exception as e:
            msg = {"result":"fail", "info":"long_data is empty"}
            self.write(json.dumps(msg))
            return

        if not LongData:
            msg = {"result":"fail", "info":"long_data is empty"}
            self.write(json.dumps(msg))
            return

        UserIdentify = str(uuid.uuid5(uuid.NAMESPACE_DNS, User.encode('utf-8')).hex)
        RemoteIP = self.request.remote_ip
        SmallURL = SmallUrlBO(self.application.config)

        try:
            ShortData = SmallURL.getShortDataFromOriginData(LongData, RemoteIP, UserIdentify)
        except Exception as e:
            if str(e) == "malicious":
                msg = {"result":"fail", "info":"malicious link:"+LongData}
                self.write(json.dumps(msg))
                return
            else:
                msg = {"result":"fail", "info":'System Error'}
                self.write(json.dumps(msg))
                logging.error(str(e))
            return

        msg = {"result":"success",
               "info":{"short_data":"http://svourl.com/"+ShortData, "long_data":LongData}}

        self.write(json.dumps(msg))


class ShortenHandler(BaseHandler):
    def get(self):
        self.addHSTS()
        self.render("index.html")

    def post(self):
        self.addHSTS()
        LongData = self.get_argument("long_data")
        if not LongData:
            self.redirect('/', permanent=False)
            return;

        RemoteIP = self.request.remote_ip

        cookieName = self.application.config["cookieName"]
        cookie = self.get_cookie(cookieName)
        NewCookie = None

        if not cookie or len(cookie) != 32 :
            NewCookie = str(uuid.uuid1().hex)
            self.set_cookie(cookieName, NewCookie)

        UserIdentify = NewCookie if cookie is None else cookie

        SmallURL = SmallUrlBO(self.application.config)

        try:
            ShortData = SmallURL.getShortDataFromOriginData(LongData, RemoteIP, UserIdentify)
        except Exception as e:
            if str(e) == "malicious":
                self.render("malicious_page.html",
                            LongData=LongData)
            else:
                self.render("error_page.html",
                    error_msg='System Error')
                logging.error(str(e))
            return

        self.render("result.html",
                    LongData=LongData,
                    ShortData=ShortData)


class RedirectHandler(BaseHandler):
    def get(self):
        RemoteIP = self.request.remote_ip
        ShortData = self.request.path.split('/')[-1]
        cookieName = self.application.config["cookieName"]
        cookie = self.get_cookie(cookieName)
        NewCookie = None

        if not cookie or len(cookie) != 32 :
            NewCookie = str(uuid.uuid1().hex)
            self.set_cookie(cookieName, NewCookie)

        UserIdentify = NewCookie if cookie is None else cookie

        SmallURL = SmallUrlBO(self.application.config)

        try:
            LongData = SmallURL.getDataFromShortData(ShortData, RemoteIP, UserIdentify)
        except Exception as e:
            if str(e) == "malicious":
                self.render("malicious_page.html",
                            LongData=ShortData)
            else:
                self.render("error_page.html",
                            error_msg='System Error')
            return

        if not LongData:
            ShortData = unquote(ShortData)
            self.render("no_result.html", ShortData=ShortData)
        else:
            if LongData['attribute'] == 'url':
                self.redirect(LongData['data'], permanent=False)
                return
            else:
                self.render("text_result.html",
                            LongData=LongData['data'])


if __name__ == "__main__":
    tornado.options.parse_command_line()
    http_server = tornado.httpserver.HTTPServer(Application(),
        ssl_options={
            "certfile": "/volume1/homes/zzzman/keys/cert1.pem",
            "keyfile": "/volume1/homes/zzzman/keys//privkey1.pem",
    })
    http_server.listen(options.port)
    tornado.ioloop.IOLoop.instance().start()
