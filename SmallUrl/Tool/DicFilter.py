import sys
import re

def main():
    filename = sys.argv[1]
    print(filename)
    fin = open(filename, encoding="utf8")
    fout = open("out_data.txt", "w", encoding="utf8")
    
    i = 0
    for line in fin:
        if len(line) <= 8 and line.strip().isalpha():
            
            fout.write(line)
            i+=1
    
    print(i)

main()