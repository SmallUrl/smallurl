import unittest
from Core import GoogleMapCore
from urllib import unquote

class GoogleMapCoreTest(unittest.TestCase):
    def test_notGoogleMap(self):
        try:                   
            st = GoogleMapCore.GoogleMapCore()
            ret = st.isGoogleMap("http://www.nccu.edu.tw")
            self.assertEqual(ret, False)
        except Exception as e:
            ret = e.args
            self.assertTrue(False)
    def test_isGoogleMap(self):
        st = GoogleMapCore.GoogleMapCore()
        ret = st.isGoogleMap("""https://www.google.com.tw/maps/place/%E5%A4%A7%E7%AB%B9%E5%9C%8B%E5%B0%8F/@25.0224352,121.2598381,15z/data=!4m2!3m1!1s0x0:0xe9e2b23ec998367b""")
        self.assertEqual(ret, True)
        ret = st.isGoogleMap("""https://www.google.co.jp/maps/place/%E3%83%8E%E3%83%BC%E3%82%B6%E3%83%B3%E3%82%AB%E3%83%B3%E3%83%88%E3%83%AA%E3%83%BC%E3%82%AF%E3%83%A9%E3%83%96%E8%B5%A4%E5%9F%8E%E3%82%B4%E3%83%AB%E3%83%95%E5%A0%B4/@36.5202852,139.0246491,12z/data=!4m2!3m1!1s0x0:0xdda75e40d95e5b11""")
        self.assertEqual(ret, True)
        ret = st.isGoogleMap("""https://www.google.co.uk/maps/place/%E4%BC%8A%E9%A6%99%E4%BF%9D%E5%9B%BD%E9%9A%9B%E3%82%AB%E3%83%B3%E3%83%84%E3%83%AA%E3%83%BC%E3%82%AF%E3%83%A9%E3%83%96/@36.5202852,139.0246491,12z/data=!4m2!3m1!1s0x0:0xf641741417940fa8""")
        self.assertEqual(ret, True)
            
    def test_getShortContent(self):
        st = GoogleMapCore.GoogleMapCore()
        ret = st.getShortContent("""https://www.google.com.tw/maps/place/%E5%A4%A7%E7%AB%B9%E5%9C%8B%E5%B0%8F/@25.0224352,121.2598381,15z/data=!4m2!3m1!1s0x0:0xe9e2b23ec998367b""")
        self.assertEqual(ret,  '%E5%A4%A7%E7%AB%B9%E5%9C%8B%E5%B0%8F')
        ret = st.getShortContent("""https://www.google.co.jp/maps/place/%E3%83%8E%E3%83%BC%E3%82%B6%E3%83%B3%E3%82%AB%E3%83%B3%E3%83%88%E3%83%AA%E3%83%BC%E3%82%AF%E3%83%A9%E3%83%96%E8%B5%A4%E5%9F%8E%E3%82%B4%E3%83%AB%E3%83%95%E5%A0%B4/@36.5202852,139.0246491,12z/data=!4m2!3m1!1s0x0:0xdda75e40d95e5b11""")
        self.assertEqual(ret, '%E3%83%8E%E3%83%BC%E3%82%B6%E3%83%B3%E3%82%AB%E3%83%B3%E3%83%88%E3%83%AA%E3%83%BC%E3%82%AF%E3%83%A9%E3%83%96%E8%B5%A4%E5%9F%8E%E3%82%B4%E3%83%AB%E3%83%95%E5%A0%B4')
        ret = st.getShortContent("""https://www.google.co.uk/maps/place/%E4%BC%8A%E9%A6%99%E4%BF%9D%E5%9B%BD%E9%9A%9B%E3%82%AB%E3%83%B3%E3%83%84%E3%83%AA%E3%83%BC%E3%82%AF%E3%83%A9%E3%83%96/@36.5202852,139.0246491,12z/data=!4m2!3m1!1s0x0:0xf641741417940fa8""")
        self.assertEqual(ret, '%E4%BC%8A%E9%A6%99%E4%BF%9D%E5%9B%BD%E9%9A%9B%E3%82%AB%E3%83%B3%E3%83%84%E3%83%AA%E3%83%BC%E3%82%AF%E3%83%A9%E3%83%96')

        ret = st.getShortContent("""https://www.google.com.tw/maps/@25.0248663,121.2742655,20z""")
        self.assertEqual(ret, None)

if __name__ == '__main__':
    unittest.main()            
