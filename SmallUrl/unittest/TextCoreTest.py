import unittest
from Core import TextCore

class TextCoreTest(unittest.TestCase):

    def test_notText(self):
        st = TextCore.TextCore()
        ret = st.isText("http://www.nccu.edu.tw")
        self.assertEqual(ret, False)

    def test_notText(self):
        st = TextCore.TextCore()
        ret = st.isText("https://bitbucket.org/SmallUrl/smallurl/issue/25/try-catch")
        self.assertEqual(ret, False)
    
    def test_isText(self):
        st = TextCore.TextCore()
        ret = st.isText("""Once you have the list of tuples, you can loop over it to do some computation for each tuple. If the pattern includes no parenthesis, then findall() returns a list of found strings as in earlier examples. If the pattern includes a single set of parenthesis, then findall() returns a list of strings corresponding to that single group. (Obscure optional feature: Sometimes you have paren ( ) groupings in the pattern, but which you do not want to extract. In that case, write the parens with a ?: at the start, e.g. (?: ) and that left paren will not count as a group result.)""")
        self.assertEqual(ret, True)

if __name__ == '__main__':
    unittest.main()            
