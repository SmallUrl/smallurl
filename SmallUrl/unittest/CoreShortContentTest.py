import unittest
from Core import CoreShortContent
import hashlib

class CoreShortContentTest(unittest.TestCase):
    def test_getShortUrl(self):
        wordlist = ["alpha", "beta", "elephant", "mouse", "move", "go", "join", "eat"]
        try:                   
            st = CoreShortContent.CoreShortContent(wordlist)
            ret = st.getShortContent("http://www.nccu.edu.tw")
            print(ret)
            self.assertNotEqual(ret, "")
        except Exception as e:
            ret = e.args
            self.assertTrue(False)
            
    def test_getContentDigest(self):
        wordlist = ["alpha", "beta", "elephant", "mouse", "move", "go", "join", "eat"]
        st = CoreShortContent.CoreShortContent(wordlist)
        test = "http://www.nccu.edu.tw"
        exp = int(hashlib.sha256(test.encode('utf-8')).hexdigest(), 16)
        ret = st.getContentDigest(test)
        self.assertEqual(exp, ret)
           
    def test_getShortUrlException(self):
        wordlist = []
        self.assertRaises(Exception, CoreShortContent.CoreShortContent(wordlist))
        wordlist = ""
        self.assertRaises(Exception, CoreShortContent.CoreShortContent(wordlist))
if __name__ == '__main__':
    unittest.main()            
