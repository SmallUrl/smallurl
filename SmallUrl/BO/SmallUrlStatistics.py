from DAO import SQLDBDAO

class SmallUrlStatistics:
    __db = None
    __connect = None
    __WebsiteDAO = None

    def __init__(self, Config):
            if self.__db is None:
                dbhost = Config["dbhost"]
                dblogin = Config["dblogin"]
                dbpassword = Config["dbpassword"]
                dbname = Config["dbname"]
                charset = Config["charset"]
                self.__db = SQLDBDAO.MySQLConnector(dbhost, dblogin, dbpassword, dbname, charset)
            if self.__connect is None:
                self.__connect = self.__db.get_connection()

    def get_total_data_count(self):
        sql = 'SELECT count(data_id) FROM data_main';
        c = self.__connect.cursor()
        try:
            c.execute(sql)
            self.__connect.commit()
        except Exception as e:
            ret = e.args
            self.__connect.rollback()
            raise Exception("SQL ERROR : " + sql + "Driver Error Msg " + ret)
        result = c.fetchone()
        return result

    def get_total_accesor_count(self):
        sql = 'SELECT count(accessor_id) FROM accessor_main';
        c = self.__connect.cursor()
        try:
            c.execute(sql)
            self.__connect.commit()
        except Exception as e:
            ret = e.args
            self.__connect.rollback()
            raise Exception("SQL ERROR : " + sql + "Driver Error Msg " + ret)
        result = c.fetchone()
        return result
