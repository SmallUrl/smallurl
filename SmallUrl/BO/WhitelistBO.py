
class WhitelistBO:
    __Whitelist = None
 
    def __init__ (self, Config):
         if self.__Whitelist is None:
             with open(Config["whitelist"]) as f:
                 self.__Whitelist = [line.rstrip() for line in f]
             f.close()

    def isInWhitelist (self, name):
        return name in self.__Whitelist
    
