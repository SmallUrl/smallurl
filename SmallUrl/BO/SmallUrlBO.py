import sys
reload(sys)
sys.setdefaultencoding('UTF-8')
from Core import CoreShortContent
import time
import hashlib
import logging
from urllib import quote
from urllib import unquote
from DAO import SQLDBDAO
from DAO import WEBSITEDAO
from Safety import JsCleaner

class SmallUrlBO:
    __db = None
    __connect = None
    __WordList = [[] for i in range(3)]
    __KeyGen = None
    __WebsiteDAO = None
    __JsCleaner = None

    def __init__(self, Config):
            # TODO config parser
            # DB link argument
            # dictionary file
            # init CoreShortContent
            # init db
            if self.__KeyGen is None:
                with open(Config["name"]) as f:
            	    self.__WordList[0] = [line.rstrip() for line in f]
                f.close()
                with open(Config["verb"]) as f:
            	    self.__WordList[1] = [line.rstrip() for line in f]
                f.close()
                with open(Config["noun"]) as f:
            	    self.__WordList[2] = [line.rstrip() for line in f]
                f.close()
                self.__KeyGen = CoreShortContent.CoreShortContent(self.__WordList)
            if self.__db is None:
                dbhost = Config["dbhost"]
                dblogin = Config["dblogin"]
                dbpassword = Config["dbpassword"]
                dbname = Config["dbname"]
                charset = Config["charset"]
                self.__db = SQLDBDAO.MySQLConnector(dbhost, dblogin, dbpassword, dbname, charset)
            if self.__connect is None:
                self.__connect = self.__db.get_connection()
            if self.__WebsiteDAO is None:
                self.__WebsiteDAO = WEBSITEDAO.WebsiteDAO()
            if self.__JsCleaner is None:
                self.__JsCleaner = JsCleaner.JsCleaner()


    def getShortDataFromOriginData(self, OriginData, UserIP, UserIdentity):
        isText = self.__isText(OriginData)
        isGoogleMap = False

        if isText == False:
            isBadSite = self.__isMaliciousSite(OriginData)
            if isBadSite is True:
                raise Exception("malicious")
            OriginData = self.__JsCleaner.strip(OriginData)
            isGoogleMap = self.__isGoogleMap(OriginData)

        Digest = str(hashlib.sha256(OriginData.encode('utf-8')).hexdigest())
        r = self.__checkOriginData(Digest)
        Cid = self.__isCreaterExist(UserIdentity)

        UrlId = ""
        ShortData = ""
        if r is not None:  
            #old data
            UrlId = r[0]
            ShortData = r[1]
            self.__updateUrlCreatCounter(UrlId)
        else:
            #New data'''
            ShortData = self.__generateShortData(OriginData, None)
            collision = self.__getOriginData(ShortData)
            while collision is not None:
                if isGoogleMap == False:
                    ShortData = self.__generateRandomShortData(OriginData)
                else:
                    ShortData = self.__generateShortData(OriginData, 1000)
                collision = self.__getOriginData(ShortData)

            UrlId = self.__insertShortData(OriginData.encode('utf-8'), ShortData, Digest)

        if False != Cid:
            #Old User
            self.__updateCreaterCounter(Cid, UserIP)
            self.__insertCreaterAndData(Cid, UrlId)
        else:
            #New User
            NewCid = self.__insertCreater(UserIP, UserIdentity)
            self.__insertCreaterAndData(NewCid, UrlId)
        
        return ShortData

    def getDataFromShortData(self, ShortData, UserIP, UserIdentity):
        ShortData = unquote(ShortData).decode('utf-8')
        r = self.__getOriginData(ShortData)
        isText = False
        LongUrl = {}

        if r is not None:
            isText = self.__isText(r[1])
            if isText == False:
                isBadSite = self.__isMaliciousSite(r[1])
                if isBadSite is True:
                    raise Exception("malicious")
                temp = self.__JsCleaner.strip(r[1])
                LongUrl.update({'data':temp, 'attribute':'url'})
            else:
                temp = r[1]
                LongUrl.update({'data':temp, 'attribute':'text'})
            UrlId = r[0] 
            self.__updateUrlAccessCounter(UrlId)
            Aid = self.__isAccessorExist(UserIdentity)
            if False != Aid:
                self.__updateAccessorCounter(Aid, UserIP)
                self.__insertAccessorAndData(Aid, UrlId)
            else:
                NewAid = self.__insertAccessor(UserIP, UserIdentity)
                self.__insertAccessorAndData(NewAid, UrlId)

        return LongUrl if r is not None else None

    def __insertShortData(self, OriginData, ShortData, Digest):
        sql = 'INSERT INTO data_main (data, data_digest, shorten_data, status) VALUES (%s, %s, %s, %s)'
        data = (OriginData, Digest, ShortData, '1')
        c = self.__connect.cursor()
        try:
            c.execute(sql, data)
            self.__connect.commit()
            UrlId = self.__connect.insert_id()
        except Exception as e:
            ret = e.args
            self.__connect.rollback()
            raise Exception("SQL ERROR : " + sql + ' '.join(data) + "Driver Error Msg " + ret)
        return UrlId

    def __isMaliciousSite(self, url):
        check = 'https://sb-ssl.google.com/safebrowsing/api/lookup?client=api&key=&appver=1.0&pver=3.1&url=' + quote(url)
        ret = self.__WebsiteDAO.ReadPage(check)
        if ret == 'malware':
           return True
        return False

    def __isCreaterExist(self, UserIdentity):
        sql = 'SELECT creater_id FROM creater_main WHERE creater_identity = %s'
        data = (UserIdentity)
        c = self.__connect.cursor()
        try:
            c.execute(sql, data)
            self.__connect.commit()
        except Exception as e:
            ret = e.args
            self.__connect.rollback()
            raise Exception("SQL ERROR : " + sql + ' '.join(data) + "Driver Error Msg " + ret)
        r = c.fetchone()
        return r[0] if r is not None else False

    def __checkOriginData(self, Digest):
        sql = """SELECT data_id, shorten_data FROM data_main WHERE data_digest = %s"""
        data = (Digest)
        c = self.__connect.cursor()
        try:
            c.execute(sql, data)
            self.__connect.commit()
        except Exception as e:
            ret = e.args
            self.__connect.rollback()
            raise Exception("SQL ERROR : " + sql + ' '.join(data) + "Driver Error Msg " + ret)
        r = c.fetchone()
        return r if r is not None else None

    def __generateShortData(self, OriginData, upper):
        ret = '' 
        isGoogleMap = self.__isGoogleMap(OriginData)

        if isGoogleMap == False:
            ret = self.__KeyGen.getRandomShortContent(OriginData)
        else:
            ret = self.__KeyGen.getGoogleMapShortContent(OriginData)
            if ret == '': #non-normal address url
                ret = self.__KeyGen.getRandomShortContent(OriginData)
       
        if upper is not None:
            ret = ret + '-' + str(self.__KeyGen.getRandomNumber(upper))

        return ret

    def __isGoogleMap(self, OriginData):
       return self.__KeyGen.isGoogleMap(OriginData)
    
    def __isText(self, OriginData):
       return self.__KeyGen.isText(OriginData)

    def __generateRandomShortData(self, OriginData):
        ret = ''
        ret = self.__KeyGen.getRandomShortContent(OriginData)
        return ret

    def __getOriginData(self, ShortData):
        sql = """SELECT data_id, data FROM data_main WHERE shorten_data = %s AND status = 1"""
        data = (ShortData)
        c = self.__connect.cursor()
        try:
            c.execute(sql, data)
            self.__connect.commit()
        except Exception as e:
            ret = e.args
            self.__connect.rollback()
            raise Exception("SQL ERROR : " + sql + ' '.join(data) + "Driver Error Msg " + ret)
        r = c.fetchone()
        return r if r is not None else None

    def __isAccessorExist(self, UserIdentity):
        sql = """SELECT accessor_id FROM accessor_main WHERE accessor_identity = %s"""
        data = (UserIdentity)
        c = self.__connect.cursor()
        try:
            c.execute(sql, data)
            self.__connect.commit()
        except Exception as e:
            ret = e.args
            self.__connect.rollback()
            raise Exception("SQL ERROR : " + sql + ' '.join(data) + "Driver Error Msg " + ret)
        r = c.fetchone()
        return r[0] if r is not None else False

    def __updateAccessorCounter(self, Aid, ip):
        sql = 'UPDATE accessor_main SET accessor_counter = accessor_counter + 1, accessor_timestamp = %s, accessor_ip = %s  WHERE accessor_id = %s'
        data = (str(int(time.time())), ip, Aid)
        c = self.__connect.cursor()
        try:
            c.execute(sql, data)
            self.__connect.commit()
        except Exception as e:
            ret = e.args
            self.__connect.rollback()
            raise Exception("SQL ERROR : " + sql + ' '.join(data) + "Driver Error Msg " + ret)
        return True

    def __updateCreaterCounter(self, Cid, ip):
        sql = 'UPDATE creater_main SET creater_counter = creater_counter + 1, creater_timestamp = %s, creater_ip = %s WHERE creater_id = %s'
        data = (str(int(time.time())), ip, Cid)
        c = self.__connect.cursor()
        try:
            c.execute(sql, data)
            self.__connect.commit()
        except Exception as e:
            ret = e.args
            self.__connect.rollback()
            raise Exception("SQL ERROR : " + sql + ' '.join(data) + "Driver Error Msg " + ret)
        return True

    def __insertAccessor(self, UserIP, UserIdentity):
        sql = 'INSERT INTO accessor_main (accessor_identity, accessor_ip, accessor_counter) VALUES (%s, %s, %s)'
        data = (UserIdentity, UserIP, '0')
        c = self.__connect.cursor()
        try:
            c.execute(sql, data)
            self.__connect.commit()
            Aid = self.__connect.insert_id()
        except Exception as e:
            ret = e.args
            self.__connect.rollback()
            raise Exception("SQL ERROR : " + sql + ' '.join(data) + "Driver Error Msg " + ret)
        return Aid

    def __insertCreater(self, UserIP, UserIdentity):
        sql = 'INSERT INTO creater_main (creater_identity, creater_ip, creater_counter) VALUES (%s, %s, %s)'
        data = (UserIdentity, UserIP, '0')
        c = self.__connect.cursor()
        try:
            c.execute(sql, data)
            self.__connect.commit()
            Cid = self.__connect.insert_id()
        except Exception as e:
            ret = e.args
            self.__connect.rollback()
            raise Exception("SQL ERROR : " + sql + ' '.join(data) + "Driver Error Msg " + ret)
        return Cid

    def __updateUrlAccessCounter(self, UrlId):
        sql = 'UPDATE data_count SET access_counter = access_counter + 1, access_timestamp = %s WHERE data_id = %s'
        data = (str(int(time.time())), UrlId)
        c = self.__connect.cursor()
        try:
            c.execute(sql, data)
            self.__connect.commit()
        except Exception as e:
            ret = e.args
            self.__connect.rollback()
            raise Exception("SQL ERROR : " + sql + ' '.join(data) + "Driver Error Msg " + ret)
        return True

    def __updateUrlCreatCounter(self, UrlId):
        sql = 'UPDATE data_count SET create_counter = create_counter + 1, access_timestamp = %s WHERE data_id = %s'
        data = (str(int(time.time())), str(UrlId))
        c = self.__connect.cursor()
        try:
            c.execute(sql, data)
            self.__connect.commit()
        except Exception as e:
            ret = e.args
            self.__connect.rollback()
            raise Exception("SQL ERROR : " + sql + ' '.join(data) + "Driver Error Msg " + ret)
        return True

    def __insertAccessorAndData(self, AccessorId, DataId):
        # low performance FIX in the future
        sql = 'REPLACE INTO accessor_and_data (data_id, accessor_id) VALUES (%s, %s)'
        data = (DataId, AccessorId)
        c = self.__connect.cursor()
        try:
            c.execute(sql, data)
            self.__connect.commit()
        except Exception as e:
            ret = e.args
            self.__connect.rollback()
            raise Exception("SQL ERROR : " + sql + ' '.join(data) + "Driver Error Msg " + ret)

    def __insertCreaterAndData(self, createrId, DataId):
        # low performance FIX in the future
        sql = 'REPLACE INTO creater_and_data (data_id, creater_id) VALUES (%s, %s)'
        data = (str(DataId), str(createrId))
        c = self.__connect.cursor()
        try:
            c.execute(sql, data)
            self.__connect.commit()
        except Exception as e:
            ret = e.args
            self.__connect.rollback()
            raise Exception("SQL ERROR : " + sql + ' '.join(data) + "Driver Error Msg " + ret)
