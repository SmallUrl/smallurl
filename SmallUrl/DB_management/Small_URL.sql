create database small_url;
use small_url;
show tables;

DROP TABLE IF EXISTS data_main;
create table data_main(
    data_id int(20) unsigned not null primary key auto_increment,
    data text character set utf8 comment 'URL for now',
    data_digest varchar(64) character set utf8 unique comment 'hash of data',
    create_timestamp timestamp default current_timestamp,
    shorten_data varchar(255) character set utf8 unique,
    status int(1) unsigned comment '0:disable, 1:available'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS data_count;
create table data_count(
    data_id int(20) unsigned not null primary key auto_increment,
    create_counter int(20) unsigned,
    access_counter int(20) unsigned,
    create_timestamp timestamp default current_timestamp,
    access_timestamp timestamp
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS creater_and_data;
create table creater_and_data(
    data_id int(20) unsigned not null,
    creater_id int(20) unsigned not null
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

insert into creater_and_data values(0001, 0001);

DROP TABLE IF EXISTS accessor_and_data;
create table accessor_and_data(
    data_id int(20) unsigned not null,
    accessor_id int(20) unsigned not null
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS creater_main;
create table creater_main(
    creater_id int(20) unsigned not null primary key auto_increment,
    creater_identity varchar(32) character set utf8,
    creater_ip varchar(15) character set utf8,
    creater_counter int(20) unsigned,
    creater_timestamp timestamp default current_timestamp on update current_timestamp
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS accessor_main;
create table accessor_main(
    accessor_id int(20) unsigned not null primary key auto_increment,
    accessor_identity varchar(32) character set utf8,
    accessor_ip varchar(15) character set utf8,
    accessor_counter int(20) unsigned,
    accessor_timestamp timestamp default current_timestamp on update current_timestamp
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

# the following is table schema change
alter table accessor_main modify accessor_identity varchar(32) character set utf8;
alter table creater_main modify creater_identity varchar(32) character set utf8;
alter table data_main modify shorten_data varchar(255) character set utf8 unique;
