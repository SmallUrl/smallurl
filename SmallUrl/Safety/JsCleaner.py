import re

class JsCleaner():
    def strip(self, input): 
        input = input.replace('\n','').replace('\r','')
        p = re.compile(r'<[^<]*?>')
        return p.sub(' ', input)

