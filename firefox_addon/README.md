# Thank you for installing SVOURL addon

## Our service website: www.svourl.com

## Tutorial

* You can access SVOURL website from the button with the S character icon on the toolbar.
* You can restore URL by selecting contents and hit on the "Decode with SVOURL" in the right click content menu.
* You can also shorten URL by selecting contents and then hit on the "Encode with SVOURL" in the right click content menu.
