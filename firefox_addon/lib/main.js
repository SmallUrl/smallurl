var buttons = require('sdk/ui/button/action');
var tabs = require("sdk/tabs");

var button = buttons.ActionButton({
    id: "svourl-link",
    label: "Visit svourl",
    icon: {
        "16": "./icon-16.png",
        "32": "./icon-32.png",
        "64": "./icon-64.png"
    },
    onClick: handleClick
});

function handleClick(state) {
    tabs.open("http://www.svourl.com/");
}

var contextMenu = require("sdk/context-menu");
var menuItem = contextMenu.Item({
    label: "SVOURL解網址",
    context: contextMenu.SelectionContext(),
    contentScript: 'self.on("click", function () {' +
        '  var text = window.getSelection().toString();' +
        '  self.postMessage(text);' +
        '});',
    onMessage: function (selectionText) {
        tabs.open('http://www.svourl.com/' + selectionText);
    }
});

var menuItem2 = contextMenu.Item({
    label: "SVOURL縮網址",
    context: contextMenu.SelectionContext(),
    contentScript: 'self.on("click", function () {' +
        '  var text = window.getSelection().toString();' +
        '  self.postMessage(text);' +
        '});',
    onMessage: function (selectionText) {
        tabs.open('http://www.svourl.com/openapi/v1/convert?id=www.test.com&long_data=' + selectionText);
    }
});
